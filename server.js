var express = require('express');
var app = express();
app.use(express.json());

app.get('/endpoints', function(req, res) {
  res.json({ payload: '/payload' });
});

function arrayDiff(a1, a2) {
  var a = [],
    diff = [];

  for (var i = 0; i < a1.length; i++) {
    a[a1[i]] = true;
  }

  for (var i = 0; i < a2.length; i++) {
    if (a[a2[i]]) {
      delete a[a2[i]];
    } else {
      a[a2[i]] = true;
    }
  }
  for (var k in a) {
    diff.push(k);
  }
  return diff;
}

app.post('/payload', function(req, res) {
  var sources = req.body.sources ? req.body.sources : [];
  var tests = req.body.tests ? req.body.tests : [];

  res.json({ test_results: arrayDiff(sources, tests) });
});

app.listen(80, function() {
  console.log('Simple runner listening on port 80!');
});
